package com.marksdu.forum.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.marksdu.forum.entity.User;
import com.marksdu.forum.entity.UserInfo;
import com.marksdu.forum.service.UserService;

/**
 * @author ljh_2017
 *
 */
@Controller
@RequestMapping("/personal")
public class PersonalController {

	@Autowired private UserService userService;
	
	@RequestMapping("/center")
	public String personalCenter(Model model) {
		model.addAttribute("user",userService.getCurrentUser());
		return "personal";
	}
	
	@RequestMapping("/message")
	@ResponseBody
	public Map<String,Object> getMessage() {
		Map<String,Object> response = new HashMap<>(20);
		User user = userService.getCurrentUser();
		if(user!=null) {
			UserInfo userinfo = user.getUserinfo();
			boolean info = userinfo!=null;
			response.put("info_status", info);
			response.put("documents_num", user.getDocuments().size());
			response.put("comments_num", user.getComments().size());
			response.put("mark", user.getMark());
			if(info) {
				response.put("userinfo", userinfo);
			}
		}
		return response;
	}
	
//	@RequestMapping("/center/{userid}")
//	public Map<String,Object> getOtherMessage(@PathVariable String userid) {
//		Map<String,Object> response = new HashMap<>();
//		UserInfo userinfo = userService.getUserInfo(userid);
//		if(userinfo!=null) {
//			response.put("info_status", userinfo!=null);
//			response.put("documents_num", userinfo.getUser().getDocuments().size());
//			response.put("comments_num", userinfo.getUser().getComments().size());
//			response.put("mark", userinfo.getUser().getMark());
//			response.put("userinfo", userinfo);
//		}
//		return response;
//	}
	
}
