package com.marksdu.forum.controller;

import java.io.IOException;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.marksdu.forum.service.HeadpicService;
import com.marksdu.forum.service.UploadImageService;
import com.marksdu.forum.service.UserService;
import com.marksdu.forum.tool.MessageEntity;

import io.swagger.annotations.ApiOperation;

/**
 * @author ljh_2017
 *
 */
@Controller
public class UploadController {

	@Resource(name = "uploadImageService")  
    private UploadImageService  uploadImageService;  
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private HeadpicService headpicService;
  
    @RequestMapping(value="/uploadImage",method={RequestMethod.POST,RequestMethod.GET})  
    @ResponseBody
    @ApiOperation(value="上传头像")
    public MessageEntity uploadImage(MessageEntity messageEntity,  
            @RequestParam(value = "file", required = true) MultipartFile file) throws IOException {  
    	MessageEntity message = uploadImageService.uploadImage(messageEntity, file);
    	headpicService.saveHead(message,userService.getCurrentUser());
        return messageEntity;  
    }  
	
}
