package com.marksdu.forum.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.marksdu.forum.entity.Document;
import com.marksdu.forum.entity.User;
import com.marksdu.forum.repository.UserRepository;
import com.marksdu.forum.service.CategoryService;
import com.marksdu.forum.service.DocumentService;
import com.marksdu.forum.service.ModelService;
import com.marksdu.forum.service.SchoolService;
import com.marksdu.forum.service.UserService;
import com.marksdu.forum.tool.ValidateCode;

/**
 * @author ljh_2017
 *
 */
@Controller
public class ShiroController {

	private static final Logger logger = LoggerFactory.getLogger(ShiroController.class);
	private static final String REMEMBERME = "rememberMe";
	private static final String MD5 = "MD5";
	
	@Autowired private UserRepository userRepository;

	@Autowired private UserService userService;

	@Autowired private DocumentService documentService;

	@Autowired private CategoryService categoryService;

	@Autowired private ModelService modelService;

	@Autowired private SchoolService schoolService;

	@RequestMapping(value="/login",method=RequestMethod.GET)
	public String login(Model model) {
		model.addAttribute("user",new User());
		return "demo";
	}

	@RequestMapping(value="/user",method=RequestMethod.GET)
	public String user(Model model) {
		Page<Document> pageInfo = documentService.allDocuments(0,20);
		model.addAttribute("total", pageInfo.getTotalElements());
		model.addAttribute("size",pageInfo.getTotalPages());
		model.addAttribute("documents", pageInfo.getContent());
		model.addAttribute("newDoc",new Document());
		model.addAttribute("cates", categoryService.getlist());
		model.addAttribute("models", modelService.listModel());
		model.addAttribute("schools", schoolService.listSchool());
		model.addAttribute("users", userService.list());
		return "user";
	}

	@RequestMapping(value="/userlist",method=RequestMethod.GET)
	public String userlist(Model model,@RequestParam("page") int page,@RequestParam("num") int num) {
		if(page>0) {
			page-=1;
		}
		Page<Document> pageInfo = documentService.allDocuments(page,num);
		model.addAttribute("total", pageInfo.getTotalElements());
		model.addAttribute("size",pageInfo.getTotalPages());
		model.addAttribute("documents", pageInfo.getContent());
		model.addAttribute("newDoc",new Document());
		return "user";
	}

	@RequestMapping(value="/register")
	public String register(Model model) {
		model.addAttribute("user",new User());
		return "register";
	}

	@RequestMapping(value="/register/user",method=RequestMethod.POST)
	public String register(@Valid User user,Model model,RedirectAttributes redirectAttributes) {
		if(userRepository.findByUsername(user.getUsername())==null) {
			redirectAttributes.addFlashAttribute("message", "注册成功");
			userService.register(user);
			return "redirect:/login";
		} else {
			redirectAttributes.addFlashAttribute("message", "该用户已被注册");
			return "redirect:/register";
		}
	}

	@RequestMapping(value="/login",method=RequestMethod.POST)
	public String login(HttpServletRequest request, @Valid User user,BindingResult bindingResult,RedirectAttributes redirectAttributes){
		if(bindingResult.hasErrors()){
			return "demo";
		}

		String validCode = request.getParameter("vcode");
		Session session = SecurityUtils.getSubject().getSession();
		String v = (String) session.getAttribute("_code");
		if(!validCode.equalsIgnoreCase(v)) {
			redirectAttributes.addFlashAttribute("validError", "验证码不正确");
			return "redirect:/login";
		}
		String username = user.getUsername();
		String password=new SimpleHash(MD5,user.getPassword(),user.getUsername(),2).toHex();
		UsernamePasswordToken token = new UsernamePasswordToken(username, password);
		//获取当前的Subject  

		//设置记住我
		if(request.getParameter(REMEMBERME)!=null) {
			token.setRememberMe(true);
		}




		Subject currentUser = SecurityUtils.getSubject();  
		try {  
			//在调用了login方法后,SecurityManager会收到AuthenticationToken,并将其发送给已配置的Realm执行必须的认证检查  
			//每个Realm都能在必要时对提交的AuthenticationTokens作出反应  
			//所以这一步在调用login(token)方法时,它会走到MyRealm.doGetAuthenticationInfo()方法中,具体验证方式详见此方法  
			logger.info("对用户[" + username + "]进行登录验证..验证开始");  
			currentUser.login(token);  
			logger.info("对用户[" + username + "]进行登录验证..验证通过");  
		}catch(UnknownAccountException uae){  
			logger.info("对用户[" + username + "]进行登录验证..验证未通过,未知账户");  
			redirectAttributes.addFlashAttribute("message", "未知账户");  
		}catch(IncorrectCredentialsException ice){  
			logger.info("对用户[" + username + "]进行登录验证..验证未通过,错误的凭证");  
			redirectAttributes.addFlashAttribute("message", "密码不正确");  
		}catch(LockedAccountException lae){  
			logger.info("对用户[" + username + "]进行登录验证..验证未通过,账户已锁定");  
			redirectAttributes.addFlashAttribute("message", "账户已锁定");  
		}catch(ExcessiveAttemptsException eae){  
			logger.info("对用户[" + username + "]进行登录验证..验证未通过,错误次数过多");  
			redirectAttributes.addFlashAttribute("message", "用户名或密码错误次数过多");  
		}catch(AuthenticationException ae){  
			//通过处理Shiro的运行时AuthenticationException就可以控制用户登录失败或密码错误时的情景  
			logger.info("对用户[" + username + "]进行登录验证..验证未通过,堆栈轨迹如下");  
			ae.printStackTrace();  
			redirectAttributes.addFlashAttribute("message", "用户名或密码不正确");  
		} 
		//验证是否登录成功  
		if(currentUser.isAuthenticated()){  
			logger.info("用户[" + username + "]登录认证通过(这里可以进行一些认证通过后的一些系统参数初始化操作)");  
			return "redirect:/user";
		}else{  
			token.clear();  
			return "redirect:/login";
		}  
	}

	@RequestMapping(value="/logout",method=RequestMethod.GET)  
	public String logout(RedirectAttributes redirectAttributes ){ 
		//使用权限管理工具进行用户的退出，跳出登录，给出提示信息
		SecurityUtils.getSubject().logout();  
		redirectAttributes.addFlashAttribute("message", "您已安全退出");  
		return "redirect:/login";
	} 

	@RequestMapping("/403")
	public String unauthorizedRole(){
		logger.info("------没有权限-------");
		return "403";
	}

	@RequestMapping("/userlist")
	public String getUserList(Map<String, Object> model){
		model.put("userList", userRepository.findAll());
		return "user";
	}

	@RequestMapping("/user/edit/{userid}")
	public String getUserList(@PathVariable int userid){
		logger.info("------进入用户信息修改-------");
		return "user_edit";
	}

	/**
	 * 获取验证码（Gif版本）
	 * @param response
	 */
	@RequestMapping(value="getGifCode",method=RequestMethod.GET)
	public void getGifCode(HttpServletResponse response,HttpServletRequest request){
		try {
			response.setHeader("Pragma", "No-cache");  
			response.setHeader("Cache-Control", "no-cache");  
			response.setDateHeader("Expires", 0);  
			response.setContentType("image/gif");  
			/**
			 * gif格式动画验证码
			 * 宽，高，位数。
			 */
			ValidateCode captcha = new ValidateCode(160,40,5,150);
			//输出
			HttpSession session = request.getSession(true);  
			//存入Session
			session.setAttribute("_code",captcha.getCode());  
			captcha.write(response.getOutputStream());
		} catch (Exception e) {
			System.err.println("获取验证码异常："+e.getMessage());
		}
	}
}
