package com.marksdu.forum.controller;

import javax.validation.Valid;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.marksdu.forum.entity.User;
import com.marksdu.forum.entity.UserInfo;
import com.marksdu.forum.service.UserService;

import io.swagger.annotations.ApiOperation;

/**
 * @author ljh_2017
 *
 */
@Controller
@RequestMapping(value="/user")
public class UserInfoController {

	@Autowired private UserService userService;
	
	@RequestMapping(value="/userinfo")
	public String userinfo() {
		return "userinfo";
	}
	
	@RequestMapping(value="/info",method=RequestMethod.GET)
	@RequiresAuthentication
	@ResponseBody
	@ApiOperation(value="获取当前用户信息")
	public UserInfo userInfo() {
		Subject currentUser = SecurityUtils.getSubject();
		UserInfo info = userService.getUserInfo((String)currentUser.getPrincipal());
		User user = userService.getCurrentUser();
		if(info==null) {
			info = new UserInfo();
			user.setUserinfo(info);
		}
		return info;
	}
	
	@RequestMapping(value="/addUserinfo",method={RequestMethod.POST,RequestMethod.GET})
	@RequiresAuthentication
	@ResponseBody
	@ApiOperation(value="添加用户个人信息", notes="完善个人信息")
	public boolean addUserInfo(@RequestParam("userinfo") String info) {
		Gson gson = new Gson();
		UserInfo userinfo = gson.fromJson(info, UserInfo.class);
		Subject currentUser = SecurityUtils.getSubject();
		if(currentUser.isAuthenticated()) {
			String userName = (String)currentUser.getPrincipal();
			userService.addOrUpdate(userName, userinfo);
			return true;
		} else {
			return false;
		}
	}
	
	@RequestMapping(value="/register",method=RequestMethod.POST)
	@RequiresUser
	@ApiOperation(value="添加用户",notes="")
	public ModelAndView addUser(@Valid User user) {
		if(user!=null) {
			userService.register(user);
			return new ModelAndView("redirect:/user");
		}
		return new ModelAndView("redirect:/403");
	}
}
