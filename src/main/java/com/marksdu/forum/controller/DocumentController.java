package com.marksdu.forum.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.marksdu.forum.entity.Document;
import com.marksdu.forum.entity.User;
import com.marksdu.forum.repository.UserRepository;
import com.marksdu.forum.service.DocumentService;

/**
 * 
 * @author ljh_2017
 *
 */
@Controller
@RequestMapping("/document")
public class DocumentController {

	@Autowired private DocumentService documentService;
	
	@Autowired private UserRepository userRepository;
	
	@RequestMapping("/{id}")
	@ResponseBody
	public Map<String,Object> documentDetail(@PathVariable long id) {
		Map<String,Object> response = new HashMap<>(20);
		Document document = documentService.getDocument(id);
		response.put("document", document);
		response.put("comment", document.getComments());
		response.put("user", document.getUser().getUserinfo());
		return response;
	}
	
	@RequestMapping(value="/handle/pub",method=RequestMethod.POST)
	public String documentPub(@Valid Document document) {
		Subject current = SecurityUtils.getSubject();
		String name = (String) current.getPrincipal();
		User user = userRepository.findByUsername(name);
		document.setUser(user);
		documentService.pubDocument(document);
		return "redirect:/user";
	}
}
