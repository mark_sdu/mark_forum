package com.marksdu.forum.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author ljh_2017
 *
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

	@RequestMapping()
	public String index() {
		return "index";
	}
	
	@RequestMapping("/design")
	public String design() {
		return "design";
	}
	
	@RequestMapping("/system")
	public String system() {
		return "system";
	}
	
	@RequestMapping("/insert")
	public String insert() {
		return "insert";
	}

}
