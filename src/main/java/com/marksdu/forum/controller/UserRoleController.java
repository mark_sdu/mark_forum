package com.marksdu.forum.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.marksdu.forum.entity.Role;
import com.marksdu.forum.entity.User;
import com.marksdu.forum.service.RoleService;
import com.marksdu.forum.service.UserService;

import io.swagger.annotations.Api;

/**
 * 用户角色Api
 * 包含授权，自定义角色添加功能接口
 * @author ljh_2017
 *
 */
@RequestMapping("/userrole")
@Api(value="用户模块，包含用户授权，自定义用户角色，用户添加，用户密码修改")
@RestController
public class UserRoleController {

	@Autowired private RoleService roleService;
	
	@Autowired private UserService userService;
	
	@RequestMapping("/grant")
	public User grant(@RequestParam("uid")int uid,@RequestParam("rid")int rid) {
		User user = userService.getUser(uid);
		Role role = roleService.getRole(rid);
		userService.grantRole(user, role);
		return user;
	}

}
