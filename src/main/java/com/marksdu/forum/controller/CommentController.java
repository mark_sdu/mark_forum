package com.marksdu.forum.controller;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.marksdu.forum.entity.Comment;
import com.marksdu.forum.entity.Document;
import com.marksdu.forum.repository.UserRepository;
import com.marksdu.forum.service.CommentService;
import com.marksdu.forum.service.DocumentService;
import com.marksdu.forum.service.UserService;

/**
 * @author ljh_2017
 *
 */
@Controller
@RequestMapping(value="/comment")
public class CommentController {

	@Autowired
	private CommentService commentService;

	@Autowired 
	private DocumentService documentService;
	
	@Autowired
	private UserRepository userService;
	
	@RequestMapping(value="/handle/pub")
	public String replyDocument(@RequestParam("id") long id,@RequestParam("context") String context) {

		Document document = documentService.getDocument(id);
		Comment comment = new Comment();
		comment.setContext(context);
		String username = (String)SecurityUtils.getSubject().getPrincipal();
		commentService.reply(document, comment,userService.findByUsername(username));
		return "redirect:/document/"+document.getId();
	}

}
