package com.marksdu.forum.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.marksdu.forum.entity.Category;
import com.marksdu.forum.entity.SubCategory;
import com.marksdu.forum.service.CategoryService;
import com.marksdu.forum.service.SubCategoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 分类
 * @author ljh_2017
 *
 */
@RestController
@Api(value="分类信息的调用接口")
@RequestMapping("/category")
public class CategoryController {

	@Autowired private CategoryService categoryService;
	
	@Autowired private SubCategoryService subCategoryService;
	
	@RequestMapping(value="/add",method=RequestMethod.POST)
	@ApiOperation(value="添加类别",notes="找到大类别，添加子类")
	public void addSubCategory(@RequestParam("categoryid")int id,@RequestParam("name")String name,@RequestParam("description")String description) {
		SubCategory sub = new SubCategory();
		sub.setCategoryName(name);
		Category category = categoryService.findById(id);
		sub.setCategory(category);
		sub.setDescription(description);
		subCategoryService.add(sub);
	}
	
	@RequestMapping(value="list",method=RequestMethod.GET)
	@ApiOperation(value="获取所有文章类别，包含子类在内",notes="文章类别")
	public List<Category> getlist() {
		return categoryService.getlist();
	}
	
	@RequestMapping(value="listOne",method=RequestMethod.GET)
	@ApiOperation(value="获取某一大类的子类别详情",notes="文章类别")
	public List<SubCategory> getSubListByCategoryId(@RequestParam("cid")int cid) {
		return subCategoryService.getCategoriesById(cid);
	}
}
