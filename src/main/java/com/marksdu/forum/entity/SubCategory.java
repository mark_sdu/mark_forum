package com.marksdu.forum.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * 
 * @author ljh_2017
 *
 */
@Entity
@Table(name="t_subcategory")
public class SubCategory {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int subid;
	
	@ManyToOne(targetEntity=Category.class,optional = true)
	@JoinColumn(name="parent_id")
	@JsonBackReference
	private Category category;
	
	@Column(nullable=false)
	private String categoryName;
	
	//定义描述
	@Column(nullable=true)
	private String description;
	
	@OneToMany(mappedBy="subCategory",fetch=FetchType.EAGER)
	@Transient
	private List<Document> documents = new ArrayList<>();

	public int getSubid() {
		return subid;
	}

	public void setSubid(int subid) {
		this.subid = subid;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public List<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
