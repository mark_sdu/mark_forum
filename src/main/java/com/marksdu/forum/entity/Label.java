package com.marksdu.forum.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * 标签
 * @author ljh_2017
 *
 */
@Entity
@Table(name="t_label")
public class Label {

	@Id
	@GeneratedValue
	private int id;
	
	//标签名
	@Column(nullable=false)
	private String labelName;
	
	@ManyToMany
	@Cascade({CascadeType.DELETE})
	@JoinTable(name = "t_doclabels", joinColumns = { @JoinColumn(name = "lid") }, inverseJoinColumns = {
			@JoinColumn(name = "did") })
	private List<Document> documents = new ArrayList<>();
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLabelName() {
		return labelName;
	}

	public void setLabelName(String labelName) {
		this.labelName = labelName;
	}

	public List<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}
	
}
