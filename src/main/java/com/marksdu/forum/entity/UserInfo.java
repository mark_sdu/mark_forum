package com.marksdu.forum.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * @author ljh_2017
 *
 */
@Entity
@Table(name="t_userinfo")
public class UserInfo {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	//网络名
	@Column(nullable=false)
	private String netName;

	@Column(nullable=false)
	private String phoneNumber;

	//姓名
	@Column(nullable=false)
	private String name;
	
	//性别
	@Column(nullable=false)
	private boolean sex;

	//身份证号码
	@Column(nullable=false)
	private String idcard;

	//学号
	@Column(nullable=false)
	private String stunum;

	//学院
	@Column(nullable=false)
	private String school;

	//专业
	@Column(nullable=false)
	private String depth;

	@Column(nullable=true)
	private String hobby;
	
	@Column(nullable=true)
	private String description;

	@JsonBackReference
	@OneToOne(optional=false)
	@Cascade({CascadeType.SAVE_UPDATE})
	@JoinColumn(name="userid",referencedColumnName="id",unique=true)
	private User user;

	@OneToOne(targetEntity=Headpic.class,mappedBy="userinfo",fetch=FetchType.EAGER,optional=true)
	@Cascade({CascadeType.DELETE})
	@Transient
	private Headpic headpic;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdcard() {
		return idcard;
	}

	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}

	public String getStunum() {
		return stunum;
	}

	public void setStunum(String stunum) {
		this.stunum = stunum;
	}

	public String getDepth() {
		return depth;
	}

	public void setDepth(String depth) {
		this.depth = depth;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Headpic getHeadpic() {
		return headpic;
	}

	public void setHeadpic(Headpic headpic) {
		this.headpic = headpic;
	}

	public String getNetName() {
		return netName;
	}

	public void setNetName(String netName) {
		this.netName = netName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public boolean isSex() {
		return sex;
	}

	public void setSex(boolean sex) {
		this.sex = sex;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
