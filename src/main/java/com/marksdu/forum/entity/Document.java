package com.marksdu.forum.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author ljh_2017
 *
 */
@Entity
@Table(name="t_document")
@JsonIgnoreProperties(value={"user"})
public class Document {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;

	@Column(nullable=false)
	private String title;

	@Lob
	@Column(nullable=false)
	private String content;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	@CreationTimestamp
	private Date createTime;

	@ManyToOne(optional=false)
	@Cascade({CascadeType.REFRESH})
	@JoinColumn(name="userid")
	private User user;

	@ManyToMany
	@Cascade({CascadeType.REFRESH})
	@JoinTable(name = "t_doclabels", joinColumns = { @JoinColumn(name = "did") }, inverseJoinColumns = {
			@JoinColumn(name = "lid") })
	private List<Label> labels = new ArrayList<>();
	/**
	 * 种类
	 */

	@OneToMany(targetEntity=Comment.class,mappedBy="document")
	@Cascade({CascadeType.DELETE})
	@JsonBackReference
	private List<Comment> comments = new ArrayList<>();

	//种类
	@ManyToOne
	@JoinColumn(name="categoryid")
	private SubCategory subCategory;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

}
