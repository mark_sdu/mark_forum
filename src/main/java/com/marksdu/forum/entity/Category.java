package com.marksdu.forum.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.annotation.Transient;

/**
 * @author ljh_2017
 *
 */
@Entity
@Table(name="t_category")
public class Category {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int cid;
	
	@Column(nullable=false)
	private String categoryName;
	
	@OneToMany(mappedBy="category",fetch=FetchType.EAGER)
	@Transient
	private Set<SubCategory> category = new HashSet<>();

	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Set<SubCategory> getCategory() {
		return category;
	}

	public void setCategory(Set<SubCategory> category) {
		this.category = category;
	}
	
}
