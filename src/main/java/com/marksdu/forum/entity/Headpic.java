package com.marksdu.forum.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * 头像
 * @author ljh_2017
 *
 */
@Entity
@Table(name="t_headpic")
public class Headpic {

	@Id
	@GeneratedValue
	private int id;

	@JsonBackReference
	@OneToOne(optional=false)
	@Cascade({CascadeType.ALL})
	@JoinColumn(name="userid",referencedColumnName="id",unique=true)
	private UserInfo userinfo;
	
	@Column(nullable=false)
	private String headpic;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public UserInfo getUserinfo() {
		return userinfo;
	}

	public void setUserinfo(UserInfo userinfo) {
		this.userinfo = userinfo;
	}

	public String getHeadpic() {
		return headpic;
	}

	public void setHeadpic(String headpic) {
		this.headpic = headpic;
	}
	
}
