package com.marksdu.forum.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.marksdu.forum.tool.UsualTool;

/**
 * @author ljh_2017
 *
 */
@Entity
@Table(name="t_user")
public class User {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Column(nullable=false)
	private String username;

	@Column(nullable=false)
	private String email;

	@Column(nullable=false)
	private String password;

	@JsonBackReference
	@OneToOne(targetEntity=UserInfo.class,mappedBy="user")
	@Cascade({CascadeType.DELETE})
	@Transient
	private UserInfo userinfo;

	@JsonBackReference
	@ManyToMany(fetch=FetchType.EAGER)//同步加载
	@Cascade({CascadeType.DELETE})
	@JoinTable(name = "t_userrole", joinColumns = { @JoinColumn(name = "uid") }, inverseJoinColumns = {
			@JoinColumn(name = "rid") })
	@Transient
	/**
	 * 角色列表
	 */
	private List<Role> roleList;

	@JsonBackReference
	@OneToMany(targetEntity=Document.class,mappedBy="user",fetch=FetchType.EAGER)
	@Cascade({CascadeType.DELETE})
	@Transient
	private Set<Document> documents = new HashSet<>();

	@Cascade({CascadeType.DELETE})
	@OneToMany(targetEntity=Comment.class,mappedBy="user",fetch=FetchType.EAGER)
	@Transient
	private Set<Comment> comments = new HashSet<>();

	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	@CreationTimestamp
	private Date createtime;

	@Temporal(TemporalType.TIMESTAMP)
	@UpdateTimestamp
	private Date lastLoginTime;

	@Column(length=1)
	private int status;

	private int mark;

	public User() {
		super();
	}

	public User(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserInfo getUserinfo() {
		return userinfo;
	}

	public void setUserinfo(UserInfo userinfo) {
		this.userinfo = userinfo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}

	public Set<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(Set<Document> documents) {
		this.documents = documents;
	}

	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Transient
	public Set<String> getRolesName() {
		List<Role> roles = getRoleList();
		Set<String> set = new HashSet<String>();
		for (Role role : roles) {
			set.add(role.getRolename());
		}
		return set;
	}

	public int getMark() {
		return UsualTool.getMark(this);
	}

	public void setMark(int mark) {
		this.mark = mark;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

}
