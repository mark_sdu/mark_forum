package com.marksdu.forum.tool;

import com.marksdu.forum.entity.Document;
import com.marksdu.forum.entity.User;

/**
 * @author ljh_2017
 *
 */
public class UsualTool {

	public static int getMark(User user){
		int docNum = user.getDocuments().size();
		int receiveComment = 0;
		for(Document doc:user.getDocuments()) {
			receiveComment+=doc.getComments().size();
		}
		int comNum = user.getComments().size();
		return docNum*3+receiveComment*1+comNum*1;
	}
	
}
