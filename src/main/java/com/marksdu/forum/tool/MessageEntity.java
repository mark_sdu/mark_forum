package com.marksdu.forum.tool;

/**
 * @author ljh_2017
 *
 */
public class MessageEntity {

	private int state;
	
	private String data;
	
	private String msg;

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	
}
