package com.marksdu.forum.serviceimpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.Permission;
import com.marksdu.forum.entity.Role;
import com.marksdu.forum.entity.User;
import com.marksdu.forum.entity.UserInfo;
import com.marksdu.forum.repository.RoleRepository;
import com.marksdu.forum.repository.UserInfoRepository;
import com.marksdu.forum.repository.UserRepository;
import com.marksdu.forum.service.UserService;

/**
 * 用户接口的实现类
 * @author ljh_2017
 *
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired UserInfoRepository userinfoRepository;

	@Autowired RoleRepository roleRepository;

	@Override
	public boolean register(User user) {
		String password=new SimpleHash("MD5",user.getPassword(),user.getUsername(),2).toHex();
		user.setPassword(password);
		Role initRole = roleRepository.findOne(2);
		List<Role> r = new ArrayList<>();
		r.add(initRole);
		user.setRoleList(r);
		User u = userRepository.save(user);
		if(u!=null) {
			return true;
		}
		return false;
	}

	@Override
	public User update(User user) {
		return userRepository.saveAndFlush(user);
	}

	@Override
	public UserInfo addOrUpdate(String username,UserInfo info) {

		User user = userRepository.findByUsername(username);
		info.setUser(user);
		info.setHeadpic(info.getHeadpic());
		userinfoRepository.save(info);
		return info;
	}

	@Override
	public UserInfo getUserInfo(String username) {
		User user = userRepository.findByUsername(username);
		return user.getUserinfo();
	}

	@Override
	public List<User> list() {
		List<User> list = userRepository.findAll();
		Collections.sort(list, new Comparator<User>(){

			@Override
			public int compare(User user1, User user2) {
				int mark1 = user1.getMark();
				int mark2 = user2.getMark();
				return mark1>=mark2?-1:1;
			}

		});
		return list.subList(0, 9);
	}

	@Override
	public User getCurrentUser() {
		Subject subject = SecurityUtils.getSubject();
		return userRepository.findByUsername((String)subject.getPrincipal());
	}

	@Override
	public User getGoodUser() {
		List<User> user = list();
		for(User u:user) {
			if(u.getUserinfo()!=null) {
				return u;
			}
		}
		return null;
	}

	@Override
	public UserInfo getUserInfo(int userid) {
		User user = userRepository.findOne(userid);
		return user.getUserinfo();
	}

	@Override
	public User grantRole(User user,Role role) {
		List<User> list = role.getUserList();
		list.add(user);
		role.setUserList(list);
		roleRepository.save(role);
		return user;
	}

	@Override
	/**
	 * 根据id获取User
	 */
	public User getUser(int id) {
		return userRepository.findOne(id);
	}

}
