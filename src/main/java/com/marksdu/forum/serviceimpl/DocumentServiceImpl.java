package com.marksdu.forum.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.Document;
import com.marksdu.forum.repository.DocumentRepository;
import com.marksdu.forum.service.DocumentService;

/**
 * @author ljh_2017
 *
 */
@Service
public class DocumentServiceImpl implements DocumentService{

	@Autowired private DocumentRepository documentRepository;
	
	@Override
	public boolean pubDocument(Document document) {
		return documentRepository.save(document)!=null;
	}

	@Override
	public Document deleteDocument(Document document) {
		return null;
	}

	@Override
	public Page<Document> allDocuments(int pageNum,int size) {
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		Pageable pageable = new PageRequest(pageNum,size,sort); 
		return documentRepository.findAll(pageable);
	}

	@Override
	public Page<Document> allDocumentsBy(String type) {
		return null;
	}

	@Override
	public Page<Document> allSerach(String key) {
		return null;
	}

	@Override
	public Document getDocument(long id) {
		return documentRepository.findOne(id);
	}



}
