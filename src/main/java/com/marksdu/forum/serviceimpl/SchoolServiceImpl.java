package com.marksdu.forum.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.School;
import com.marksdu.forum.repository.SchoolRepository;
import com.marksdu.forum.service.SchoolService;

/**
 * @author ljh_2017
 *
 */
@Service
public class SchoolServiceImpl implements SchoolService {

	@Autowired private SchoolRepository schoolRepository;
	
	@Override
	public List<School> listSchool() {
		return schoolRepository.findAll();
	}

}
