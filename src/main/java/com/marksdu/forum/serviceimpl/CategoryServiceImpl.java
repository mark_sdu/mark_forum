package com.marksdu.forum.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.Category;
import com.marksdu.forum.repository.CategoryRepository;
import com.marksdu.forum.service.CategoryService;

/**
 * @author ljh_2017
 *
 */
@Service
public class CategoryServiceImpl implements CategoryService{

	@Autowired private CategoryRepository categoryRepository;
	
	@Override
	public List<Category> getlist() {
		return categoryRepository.findAll();
	}

	@Override
	public Category findById(int categoryid) {
		return categoryRepository.findOne(categoryid);
	}

}
