package com.marksdu.forum.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.Category;
import com.marksdu.forum.entity.SubCategory;
import com.marksdu.forum.repository.CategoryRepository;
import com.marksdu.forum.repository.SubCategoryRepository;
import com.marksdu.forum.service.SubCategoryService;

/**
 * 
 * @author ljh_2017
 *
 */
@Service
public class SubCategoryServiceImpl implements SubCategoryService{

	@Autowired private SubCategoryRepository subCategoryRepository;
	
	@Autowired private CategoryRepository categoryRepository;
	
	@Override
	public void add(SubCategory subCategory) {
		subCategoryRepository.save(subCategory);
	}

	@Override
	public void delete(int id) {
		subCategoryRepository.delete(id);
	}

	@Override
	public List<SubCategory> getCategories() {
		return subCategoryRepository.findAll();
	}

	@Override
	public List<SubCategory> getCategoriesById(int id) {
		Category cate = categoryRepository.findOne(id);
		return subCategoryRepository.findByCategory(cate);
	}

}
