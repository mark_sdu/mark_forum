package com.marksdu.forum.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.Model;
import com.marksdu.forum.repository.ModelRepository;
import com.marksdu.forum.service.ModelService;

/**
 * @author ljh_2017
 *
 */
@Service
public class ModelServiceImpl implements ModelService {

	@Autowired private ModelRepository modelRepository;
	
	@Override
	public List<Model> listModel() {
		return modelRepository.findAll();
	}

}
