package com.marksdu.forum.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.Permission;
import com.marksdu.forum.entity.Role;
import com.marksdu.forum.repository.PermissionRepository;
import com.marksdu.forum.repository.RoleRepository;
import com.marksdu.forum.service.RoleService;

/**
 * 用户角色接口
 * 实现自定义角色添加
 * 角色权限修改
 * @author ljh_2017
 *
 */
@Service
public class RoleServiceImpl implements RoleService{

	@Autowired private RoleRepository roleRepository;
	
	@Autowired private PermissionRepository permissionRepository;
	
	@Override
	public boolean save(Role role) {
		return roleRepository.save(role)!=null;
	}

	@Override
	public boolean addPermission(Role role, Permission permission) {
		List<Permission> plist = role.getPermissionList();
		plist.add(permission);
		role.setPermissionList(plist);
		return save(role);
	}

	@Override
	public Role getRole(int id) {
		return roleRepository.findOne(id);
	}

}
