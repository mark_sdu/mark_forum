package com.marksdu.forum.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.Comment;
import com.marksdu.forum.entity.Document;
import com.marksdu.forum.entity.User;
import com.marksdu.forum.repository.CommentRepository;
import com.marksdu.forum.service.CommentService;

/**
 * @author ljh_2017
 *
 */
@Service
public class CommentServiceImpl implements CommentService {

	@Autowired private CommentRepository commentRepository;
	
	@Override
	public boolean reply(Document document, Comment comment,User user) {
		comment.setDocument(document);
		comment.setUser(user);
		return commentRepository.save(comment) != null;
	}

	@Override
	public Comment delete(Comment comment) {
		return null;
	}

	@Override
	public Page<Comment> allComments(Document document) {
		return null;
	}

}
