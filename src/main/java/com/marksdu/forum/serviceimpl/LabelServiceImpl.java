package com.marksdu.forum.serviceimpl;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.Document;
import com.marksdu.forum.entity.Label;
import com.marksdu.forum.service.LabelService;

/**
 * @author ljh_2017
 *
 */
@Service
public class LabelServiceImpl implements LabelService {

	@Override
	public List<Label> getLabels() {
		return null;
	}

	@Override
	public Page<Document> getDocumentsByLabelName(String name) {
		return null;
	}

}
