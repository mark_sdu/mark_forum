package com.marksdu.forum.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.Headpic;
import com.marksdu.forum.entity.User;
import com.marksdu.forum.repository.HeadpicRepository;
import com.marksdu.forum.service.HeadpicService;
import com.marksdu.forum.tool.MessageEntity;

/**
 * @author ljh_2017
 *
 */
@Service
public class HeadpicServiceImpl implements HeadpicService{

	@Autowired
	private HeadpicRepository headpicRepository;
	
	@Override
	public boolean saveHead(MessageEntity entity,User user) {
		Headpic pic = user.getUserinfo().getHeadpic();
		if(pic==null) {
			pic = new Headpic();
			pic.setUserinfo(user.getUserinfo());
		}
		String route = entity.getData();
		int index = route.lastIndexOf('/');
		pic.setHeadpic(entity.getData().substring(index));
		headpicRepository.save(pic);
		return true;
	}

}
