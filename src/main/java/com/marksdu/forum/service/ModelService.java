package com.marksdu.forum.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.Model;

/**
 * @author ljh_2017
 *
 */
@Service
public interface ModelService {

	/**
	 * 获取所有model
	 * @return
	 */
	List<Model> listModel();
	
}
