package com.marksdu.forum.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.SubCategory;

/**
 * 子类种类
 * @author ljh_2017
 *
 */
/**
 * 博客发帖种类子类接口
 * 包含后台自定义增加子类，删除子类
 * 根据母类获取子类
 * 获取所有子类信息
 * 等功能
 * @author ljh_2017
 *
 */
@Service
public interface SubCategoryService {

	/**
	 * 添加一个子类
	 * 
	 * @param subCategory 子类信息
	 */
	void add(SubCategory subCategory);
	
	
	/**
	 * 根据id删除类别
	 * @param id 子类id
	 */
	void delete(int id);
	
	/**
	 * 根据母类id获取其下所有子类信息
	 * @param id 母类id categoryid
	 * @return
	 */
	List<SubCategory> getCategoriesById(int id);
	
	/**
	 * 获取全部子类信息
	 * @return
	 */
	List<SubCategory> getCategories();
	
	
	
}
