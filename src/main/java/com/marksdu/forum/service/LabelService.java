package com.marksdu.forum.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.Document;
import com.marksdu.forum.entity.Label;

/**
 * @author ljh_2017
 *
 */
@Service
public interface LabelService {

	/**
	 * 根据标签名数组寻找
	 * @param names
	 * @return
	 */
	List<Label> getLabels();
	
	/**
	 * 根据标签查找Document
	 * @param name
	 * @return
	 */
	Page<Document> getDocumentsByLabelName(String name);
}
