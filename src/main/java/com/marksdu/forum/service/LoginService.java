package com.marksdu.forum.service;

import com.marksdu.forum.entity.User;

/**
 * @author ljh_2017
 *
 */
public interface LoginService {

	//登录用户
	/**
	 * 用户登录
	 * @param username 用户名
	 * @param password 密码
	 * @return
	 */
	User login(String username,String password);
	
}
