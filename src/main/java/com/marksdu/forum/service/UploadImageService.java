package com.marksdu.forum.service;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.marksdu.forum.tool.DateDealwith;
import com.marksdu.forum.tool.MessageEntity;

/**
 * @author ljh_2017
 *
 */
@Service("uploadImageService")
public class UploadImageService {

	@Value("${web.upload-path}")  
	private String  path;  

	@Value("${server.context-path}")  
	private String  contxtePath;  
	
	private static final String BMP = "BMP";
	private static final String JPG = "JPG";
	private static final String JPEG = "JPEG";

	public MessageEntity uploadImage(MessageEntity messageEntity,  
			@RequestParam(value = "file", required = true) MultipartFile file) throws IOException {  
		if (file != null) {  
			if (file.getName() != null || "".equals(file.getName())) {  
				String[] name = file.getContentType().split("/");  
				if (BMP.equals(name[name.length - 1]) || JPG.equals(name[name.length - 1])  
						|| JPEG.equals(name[name.length - 1]) || BMP.equals(name[name.length - 1])  
						|| JPG.equals(name[name.length - 1]) || JPEG.equals(name[name.length - 1])) {  
					// 物理地址  
					file.transferTo(new File(path + DateDealwith.getSHC()));  
					messageEntity.setState(0);  
					messageEntity.setMsg("上传成功!");  
					// 网络地址  
					messageEntity.setData(contxtePath + "/" + DateDealwith.getSHC());  
					// 先删除原来的文件，再将网络地址写入数据库  
					return messageEntity;  
				} else {  
					messageEntity.setState(1);  
					messageEntity.setMsg("格式不正确!");  
				}  
			} else {  
				messageEntity.setState(1);  
				messageEntity.setMsg("请选择文件!");  
			}  
		} else {  
			messageEntity.setState(1);  
			messageEntity.setMsg("请选择文件!");  
		}  
		return messageEntity;  

	}  

}
