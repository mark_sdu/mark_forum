package com.marksdu.forum.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.School;

/**
 * @author ljh_2017
 *
 */
@Service
public interface SchoolService {

	/**
	 * 查询数据库所有学校
	 * @return
	 */
	List<School> listSchool();
	
}
