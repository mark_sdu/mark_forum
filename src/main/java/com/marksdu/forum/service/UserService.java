package com.marksdu.forum.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.Role;
import com.marksdu.forum.entity.User;
import com.marksdu.forum.entity.UserInfo;

/**
 * 用户接口
 * @author ljh_2017
 *
 */
@Service
public interface UserService {

	//注册用户
	/**
	 * 注册用户
	 * @param user 用户
	 * @return
	 */
	boolean register(User user);
	
	//修改密码
	/**
	 * 修改用户
	 * 修改用户密码
	 * @param user
	 * @return
	 */
	User update(User user);
	
	//授予角色
	/**
	 * 授予用户角色权限
	 * @param user 用户
	 * @param role 用户角色
	 * @return
	 */
	User grantRole(User user,Role role);
	
	//完善信息
	/**
	 * 完善用户信息
	 * @param username 用户名
	 * @param info 用户信息
	 * @return
	 */
	UserInfo addOrUpdate(String username,UserInfo info);
	
	//获取个人信息
	/**
	 * 获取个人信息
	 * @param username 用户名
	 * @return
	 */
	UserInfo getUserInfo(String username);

	/**
	 * 获取个人信息
	 * @param userid 用户id
	 * @return
	 */
	UserInfo getUserInfo(int userid);
	
	/**
	 * 获取当前登录的用户
	 * @return
	 */
	User getCurrentUser();
	
	/**
	 * 获取点赞的用户
	 * @return
	 */
	User getGoodUser();
	
	/**
	 * 获取指定用户
	 * @param id 用户id
	 * @return
	 */
	User getUser(int id);
	
	/**
	 * 获取所有用户信息
	 * @return
	 */
	List<User> list();
}
