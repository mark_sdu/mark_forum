package com.marksdu.forum.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.Comment;
import com.marksdu.forum.entity.Document;
import com.marksdu.forum.entity.User;

/**
 * 评论接口
 * @author ljh_2017
 *
 */
@Service
public interface CommentService {

	//回复帖子
	/**
	 * 回帖
	 * @param document
	 * @param comment
	 * @param user
	 * @return
	 */
	boolean reply(Document document,Comment comment,User user);
	
	//删除评论
	/**
	 * 删除评论
	 * @param comment
	 * @return
	 */
	Comment delete(Comment comment);
	
	//根据帖子获取所有评论
	/**
	 * 根据帖子获取所有评论
	 * @param document
	 * @return
	 */
	Page<Comment> allComments(Document document);
	
}
