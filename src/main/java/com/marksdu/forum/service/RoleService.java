package com.marksdu.forum.service;

import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.Permission;
import com.marksdu.forum.entity.Role;

/**
 * @author ljh_2017
 *
 */
@Service
/**
 * 角色服务
 * @author ljh_2017
 *
 */
public interface RoleService {

	/**
	 * 保存修改用户角色
	 * @param role 角色
	 * @return
	 */
	public boolean save(Role role);
	
	/**
	 * 为某角色添加权限
	 * @param role 角色
	 * @param permission 权限
	 * @return
	 */
	public boolean addPermission(Role role,Permission permission);
	
	
	/**
	 * 根据id获取用户角色
	 * @param id 角色id
	 * @return
	 */
	public Role getRole(int id);
	
}
