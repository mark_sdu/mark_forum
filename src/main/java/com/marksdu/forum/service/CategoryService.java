package com.marksdu.forum.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.Category;

@Service
/**
 * 
 * @author ljh_2017
 *
 */
public interface CategoryService {

	/**
	 * 获取种类列表
	 * @return
	 */
	List<Category> getlist();
	
	/**
	 * 根据id获取Category
	 * @param categoryid
	 * @return
	 */
	Category findById(int categoryid);
}
