package com.marksdu.forum.service.admin;

import org.springframework.stereotype.Service;

import com.marksdu.forum.service.CommentService;

/**
 * 后台管理员的接口
 * @author ljh_2017
 *
 */
@Service
public interface AdminCommentService extends CommentService{

}
