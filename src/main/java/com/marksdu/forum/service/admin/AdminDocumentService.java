package com.marksdu.forum.service.admin;

import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.Document;
import com.marksdu.forum.service.DocumentService;

/**
 * @author ljh_2017
 *
 */
@Service
public interface AdminDocumentService extends DocumentService{
	
	/**
	 * 给document 授权
	 * @param document
	 * @param grant
	 */
	void grant(Document document,int grant);
	
}
