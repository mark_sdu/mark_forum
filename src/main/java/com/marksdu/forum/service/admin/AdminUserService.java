package com.marksdu.forum.service.admin;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.User;

/**
 * @author ljh_2017
 *
 */
@Service
public interface AdminUserService {

	//添加User
	/**
	 * 添加User
	 * @param user
	 * @return
	 */
	boolean add(User user);
	
	//删除User
	/**
	 * 删除User
	 * @param user
	 * @return
	 */
	User deleteUser(User user);
	
	//修改User
	/**
	 * 修改User
	 * @param user
	 * @return
	 */
	User updateUser(User user);
	
	//按页查询User
	/**
	 * 按页查询User
	 * @return
	 */
	Page<User> listUser();
	
	//授予User权限
	/**
	 * 授权
	 * @param user
	 * @param grant
	 */
	void grant(User user, int grant);
	
}
