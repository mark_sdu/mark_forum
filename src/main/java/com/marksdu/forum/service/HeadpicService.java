package com.marksdu.forum.service;

import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.User;
import com.marksdu.forum.tool.MessageEntity;

/**
 * @author ljh_2017
 *
 */
@Service
public interface HeadpicService {

	/**
	 * 保存用户头像
	 * @param entity 头像信息载体
	 * @param user 用户
	 * @return
	 */
	boolean saveHead(MessageEntity entity,User user);
	
}
