package com.marksdu.forum.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.marksdu.forum.entity.Document;

/**
 * 帖子接口
 * @author ljh_2017
 *
 */
@Service
public interface DocumentService {

	//发布帖子
	/**
	 * 发帖
	 * @param document
	 * @return
	 */
	boolean pubDocument(Document document);

	//删除帖子
	/**
	 * 删帖
	 * @param document
	 * @return
	 */
	Document deleteDocument(Document document);
	
	//获得帖子
	/**
	 * 查指定id贴
	 * @param id
	 * @return
	 */
	Document getDocument(long id);

	//获得全部帖子
	/**
	 * 分页获取帖子列表
	 * @param pageNum
	 * @param size
	 * @return
	 */
	Page<Document> allDocuments(int pageNum,int size);

	//按条件获取帖子
	/**
	 * 按条件获取帖子
	 * @param type
	 * @return
	 */
	Page<Document> allDocumentsBy(String type);

	//按关键字查询
	/**
	 * 关键字查询
	 * @param key
	 * @return
	 */
	Page<Document> allSerach(String key);
}
