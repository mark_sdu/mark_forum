package com.marksdu.forum.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marksdu.forum.entity.Role;

/**
 * 用户角色Repository
 * @author ljh_2017
 *
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Integer>{

}
