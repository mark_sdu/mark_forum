package com.marksdu.forum.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marksdu.forum.entity.Label;


/**
 * 标签Repository
 * @author ljh_2017
 *
 */
@Repository
public interface LabelRepository extends JpaRepository<Label, Integer> {
	
	/**
	 * 根据标签名获取标签
	 * @param name
	 * @return
	 */
	Label findByLabelName(String name);
	
}
