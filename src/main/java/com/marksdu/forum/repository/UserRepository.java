package com.marksdu.forum.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marksdu.forum.entity.User;

@Repository
/**
 * 
 * @author ljh_2017
 *
 */
public interface UserRepository extends JpaRepository<User, Integer>{

	/**
	 * 根据username查找User
	 * @param username
	 * @return
	 */
	User findByUsername(String username);
	
}
