package com.marksdu.forum.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marksdu.forum.entity.Category;

/**
 * 种类接口
 * @author ljh_2017
 *
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer>{
	
}
