package com.marksdu.forum.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marksdu.forum.entity.Headpic;

/**
 * 头像接口
 * @author ljh_2017
 *
 */
@Repository
public interface HeadpicRepository extends JpaRepository<Headpic, Integer>{
	
}
