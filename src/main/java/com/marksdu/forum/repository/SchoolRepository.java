package com.marksdu.forum.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marksdu.forum.entity.School;

/**
 * 学校Repository
 * @author ljh_2017
 *
 */
@Repository
public interface SchoolRepository extends JpaRepository<School,Integer>{

}
