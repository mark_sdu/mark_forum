package com.marksdu.forum.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marksdu.forum.entity.Permission;

/**
 * 权限仓库
 * @author ljh_2017
 *
 */
@Repository
public interface PermissionRepository extends JpaRepository<Permission, Integer>{

}
