package com.marksdu.forum.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marksdu.forum.entity.Category;
import com.marksdu.forum.entity.SubCategory;

@Repository
/**
 * 
 * @author ljh_2017
 *
 */
public interface SubCategoryRepository extends JpaRepository<SubCategory,Integer>{

	/**
	 * 根据category寻找它的子类
	 * @param category
	 * @return
	 */
	List<SubCategory> findByCategory(Category category);

}
