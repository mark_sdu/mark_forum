package com.marksdu.forum.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marksdu.forum.entity.Comment;

/**
 * 评论接口
 * @author ljh_2017
 *
 */
@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
	
}
