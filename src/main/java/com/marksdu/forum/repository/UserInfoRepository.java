package com.marksdu.forum.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marksdu.forum.entity.UserInfo;

/**
 * @author ljh_2017
 *
 */
@Repository
public interface UserInfoRepository extends JpaRepository<UserInfo, Integer>{
 
	 
}
