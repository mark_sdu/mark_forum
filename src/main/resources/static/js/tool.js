var ue = UE.getEditor('editor');

function submitForm() {
	var title = $("#title").val();
	var content = $("textarea[name='content']").val();
	var flag = true;
	if(title.trim().length<5) {
		$(".title-wanning").css("display","block");
		flag = false;
	} else
		$(".title-wanning").css("display","none");
	if(content.trim().length<1 || content.trim()=="") {
		$(".content-wanning").css("display","block");
		flag = false
	} else
		$(".content-wanning").css("display","none");
	if(flag) {
		$("#form").submit();
	}
}