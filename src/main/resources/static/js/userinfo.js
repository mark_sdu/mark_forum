var info;

window.onload = function() {

	$.ajax({
		url:"info",
		method:"get",
		type:"json",
		success:function(data) {
			if(data) {
				console.info(data);
				info = data;
				if(info){
					reload();
				}
			}
		}
	});
	
}


function reload() {
	console.info("load");
	if(!info)
		load();
	if(info) {
		$.each(info, function(i, elt) {
			if(i=="headpic") {
				$("#imgid").attr("src","/marksdu/"+elt.headpic);
				$("#subtn").removeAttr('disabled');
				$("#btn").val("修改头像");
			} else
				$('#'+i).val(elt);
		})
	}
}

function infosubmit() {
	delete info.headpic;
	$.ajax({
		method:"POST",
		url:"addUserinfo",
		data:{"userinfo":JSON.stringify(info)},
		dataType:"json",
		success:function(data){
			console.info(data);
			if(data){
				alert("个人信息填写成功");
			} else
				alert("提交失败");
		}
	});
}

function load() {
	$.each(info,function(key,elt){
		var value = $('#'+key).val();
		if(value && value.length>0) {
			info[key] = value;
		}
	});
}

function downtab(target) {
	$(".information-box .show-box").eq(target-1).removeClass("active");
	$(".information-box .show-box").eq(target).addClass("active");
	reload();
}

function uptab(target) {
	$(".information-box .show-box").eq(target).removeClass("active");
	$(".information-box .show-box").eq(target-1).addClass("active");
	reload();
}