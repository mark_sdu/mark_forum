$(document).ready(function() {  
	$("#btn").click(function () {  
		if(confirm("确认上传？")){  
			var imagePath = $("#uploadimage").val();    
			if (imagePath == "") {    
				alert("please upload image file",2);  
				return false;    
			}    
			var strExtension = imagePath.substr(imagePath.lastIndexOf('.') + 1);    
			if (strExtension!='jpg') {   
				if (strExtension!='bmp') {   
					if (strExtension!='png') {   
						alert("please upload file that is a image",2);    
						return false;   
					}  
				}  
			}  
			$("#upload").ajaxSubmit({    
				type : 'POST',    
				url : '../uploadImage',    
				success : function(result) {
					var data = $.parseJSON(result.replace(/<.*?>/ig,""));
					console.info(data);
					if(data.state=='0'){  
						$("#imgDiv").empty();  
						$("#imgDiv").html('<img src="'+data.data+'" style="width:140px;height:140px;"/>');  
						$("#imgDiv").show();  
						$("#uploadimage").val("");  
						$("#subtn").removeAttr('disabled');
						$("#btn").css("display", "none");
					}else{  
						console.info(data.msg+":"+data.data);  
					}  
				},    
				error : function() {    
					alert("上传失败，请检查网络后重试,上传文件太大");    
				}    
			});  
		}  
	})  
});   